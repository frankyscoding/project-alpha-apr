from django.shortcuts import render, redirect
from tasks.forms import CreateTask
from tasks.models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
    context = {
        "form": form,
    }
    return render(request, "tasks/createtask.html", context)


def task_list(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"task_list": tasks}
    return render(request, "tasks/list.html", context)
